# Allow number of jobs to be set
THREADS ?= 1

deb_dist = ./deb-dist

# Delte all build and dist folders
clean:
	@rm -r ./build 2> /dev/null || true
	@rm -r ${deb_dist} 2> /dev/null || true

# Compile and install the core module
install:
	@mkdir ./build 2> /dev/null || true
	cmake -B./build -H.
	make --no-print-directory --directory=./build -j${THREADS}

# Create a deb package
package: install
	@mkdir ${deb_dist} 2> /dev/null || true
	make --no-print-directory --directory=./build package
	@mv ./build/*.deb ${deb_dist}/
