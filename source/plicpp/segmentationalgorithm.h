/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef CPP_PLI_SEGMENTATION_ALGORITHM_H
#define CPP_PLI_SEGMENTATION_ALGORITHM_H

#include <cstdlib>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <boost/algorithm/string.hpp>

#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

#include <MSeg/interface/DataTypes.h>
#include <MSeg/interface/SegmentationAlgorithmInterface.h>

#include "dataexchangeproxy.h"

namespace plicpp
{
    class SegmentationAlgorithm
        : public mseg::SegmentationAlgorithmInterface
    {

    private:
        Json::Value parameters;

    protected:
        plicpp::DataExchangeProxy* data;
        std::string name = "";
        bool requiresTraining = false;
        mseg::Granularity trainingGranularity = mseg::Granularity::Medium;

    public:
        SegmentationAlgorithm();
        virtual ~SegmentationAlgorithm();

        plicpp::DataExchangeProxy* getData();

        virtual void train();
        virtual void resetTraining();
        virtual void segment() = 0;

        virtual bool internal_requiresTraining(const ::Ice::Current& = ::Ice::Current());
        virtual void internal_resetTraining(const Ice::Current& c = ::Ice::Current());
        virtual mseg::Granularity internal_getTrainingGranularity(const Ice::Current& c = ::Ice::Current());
        virtual void internal_train(const ::Ice::Current& = ::Ice::Current());
        virtual void internal_segment(const Ice::Current& c = ::Ice::Current());
        virtual std::string internal_getName(const Ice::Current& c = ::Ice::Current());

        virtual std::string internal_getParameters(const Ice::Current& c = ::Ice::Current());
        virtual void internal_setParameters(const std::string&, const Ice::Current& c = ::Ice::Current());

        void registerIntParameter(std::string name, std::string description, int defaultValue, int min, int max);
        void registerFloatParameter(std::string name, std::string description, double defaultValue, int decimals, double min, double max);
        void registerBoolParameter(std::string name, std::string description, bool defaultValue);
        void registerStringParameter(std::string name, std::string description, std::string defaultValue);
        void registerJsonParameter(std::string name, std::string description, Json::Value defaultValue);

        bool getBoolParameter(std::string name);
        int getIntParameter(std::string name);
        double getFloatParameter(std::string name);
        std::string getStringParameter(std::string name);
        Json::Value getJsonParameter(std::string name);

    private:

        bool isParameterRegistered(std::string name);
        void setParameterValue(std::string name, std::string value);
        int findParameterByName(std::string name);

    };
}

#endif
